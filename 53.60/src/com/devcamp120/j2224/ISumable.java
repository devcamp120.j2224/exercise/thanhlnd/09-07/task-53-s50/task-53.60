package com.devcamp120.j2224;

public interface ISumable {
   String getSum();
}
